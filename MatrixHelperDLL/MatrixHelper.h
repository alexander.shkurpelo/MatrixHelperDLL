#pragma once
#include <vector>
#include <memory>


template <class T> 
T getNegLinesSum(const std::shared_ptr<std::vector<std::vector<T>>> matrix);

template _declspec(dllexport) int getNegLinesSum(const std::shared_ptr<std::vector<std::vector<int>>> matrix);

template _declspec(dllexport) double getNegLinesSum(const std::shared_ptr<std::vector<std::vector<double>>> matrix);

template _declspec(dllexport) float getNegLinesSum(const std::shared_ptr<std::vector<std::vector<float>>> matrix);

template _declspec(dllexport) size_t getNegLinesSum(const std::shared_ptr<std::vector<std::vector<size_t>>> matrix);
