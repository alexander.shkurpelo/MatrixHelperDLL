#include "MatrixHelper.h"
template <class T>
T getNegLinesSum(const std::shared_ptr<std::vector<std::vector<T>>> matrix)
{
	T result = 0;
	size_t matrixSize = matrix->size();

	for (size_t line = 0; line < matrixSize; ++line)
	{
		if ((*matrix)[line][line] < 0)
			for (size_t row = 0; row < matrixSize; ++row)
				result += (*matrix)[line][row];
	}

	return result;
}