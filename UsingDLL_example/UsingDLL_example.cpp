// UsingDLL_example.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <ostream>
#include <iostream>
#include <vector>
#include <memory>
#include <random>

typedef std::vector<int> MatrixLine;
typedef std::vector<MatrixLine> Matrix;
typedef std::shared_ptr<Matrix> MatrixPtr;

template <class T>
__declspec(dllimport) T getNegLinesSum(const std::shared_ptr<std::vector<std::vector<T>>>);

void printMatrix(const Matrix& matrix, const size_t size)
{
	for (size_t line = 0; line < size; ++line)
	{
		for (size_t row = 0; row < size; ++row)
			std::cout << matrix[line][row] << "\t";
		std::cout << std::endl;
	}
}

void fillMatrix(Matrix& matrix, const size_t size, const int min, const int max)
{
	std::random_device rd;
	std::mt19937 eng(rd()); 
	std::uniform_int_distribution<int> rndEl(min, max);

	for (size_t line = 0; line < size; ++line)
		for (size_t row = 0; row < size; ++row)
			matrix[line][row] = rndEl(eng);
}

int main()
{
	const size_t size = 5;
	MatrixPtr matrix = std::make_shared<Matrix>(size, MatrixLine(size));

	fillMatrix(*matrix, size, -5, 5);
	printMatrix(*matrix, size);

	auto sum = getNegLinesSum(matrix);
	std::cout << "\n\nSum of elements in lines with negative element on diagonal: " << sum << std::endl;

	system("pause");
    return 0;
}

